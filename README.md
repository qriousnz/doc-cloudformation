Cloudformation Scripts for DOC Analytics Project
====================================================================

This project has a few Cloudformation template scripts to provision the resources in AWS for DOC Project.
# Prerequisites

- AWS CLI
- AWS DOC Account and a role to be assumed which has Admin permission
- DOC Sandpit/Test Account configuration

# Cloudformation Templates:
Avneet used to create one single template to contain all the artifacts for DOC analytics project. To make it easy to manage and clear to read, I split it into 3 files:
- templates
   - analytics-1-lambda.yaml
   - analytics-2-dynamodb.yaml
   - analytics-3-s3.yaml
   
As these components have dependencies, the execution order should be remained. 

# Configuration for AWS CLI configuration

Basically, the following is an example configuration for AWS CLI

```
~/.aws/credentials
[default]
aws_access_key_id = AKIAJ5O...
aws_secret_access_key = A2zHpIn...

[doc-test]
aws_access_key_id = AKIAIEHU4GU...
aws_secret_access_key = JLn8m+RV4Kyr...

~/.aws/config
[default]
region = ap-southeast-2

[profile doc-test]
region = ap-southeast-2
role_arn = arn:aws:iam::<doc-test-account-num>:role/<role-name>
source_profile = default
```
For each deployment environment, there should be a set of parameters defined for that. These deployments are defined under the **config** directory
You can define as many endearments as you need, just copy the debug directory to a new one and update the parameters in the following files accordingly.
  - debug
    - analytics-1-lambda.yaml
    - analytics-2-dynamodb.yaml
    - analytics-3-s3.yaml
  - dev
  - prod
  - uat

# Usage

There is a bash script to help invoke the Cloudformation script: create.stack.sh. Update the vaiables in it before executing it.

```
  cd config/debug/
  ./create.stack.sh
```
