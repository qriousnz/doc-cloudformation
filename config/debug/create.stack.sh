#!/usr/bin/env bash

export stage="debug"
export stackprefix="project-analytics"

export SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
export TEMPLATEPATH=$SCRIPTPATH/../../templates
#echo $SCRIPTPATH, $TEMPLATEPATH

python $SCRIPTPATH/../../convertYaml2Json.py

# Step 1 - create lambda functions
aws --profile doc-test cloudformation create-stack --stack-name "$stackprefix-$stage-lambda" --template-body file://$TEMPLATEPATH/analytics-1-lambda.yaml --parameters file://$SCRIPTPATH/analytics-1-lambda.json --capabilities CAPABILITY_NAMED_IAM

# Step 2 - create dynamo db tables
# aws --profile doc-test cloudformation create-stack --stack-name "$stackprefix-$stage-dynamodb" --template-body file://$TEMPLATEPATH/analytics-2-dynamodb.yaml --parameters file://$SCRIPTPATH/analytics-2-dynamodb.json --capabilities CAPABILITY_NAMED_IAM

# Step 3 - create s3 buckets and notifications for Lambda functions
#aws --profile doc-test cloudformation create-stack --stack-name "$stackprefix-$stage-s3" --template-body file://$TEMPLATEPATH/analytics-3-s3.yaml --parameters file://$SCRIPTPATH/analytics-3-s3.json --capabilities CAPABILITY_NAMED_IAM
