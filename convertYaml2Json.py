#!/usr/bin/env python
import os
import sys
import yaml
import json


def get_script_path():
    return os.path.dirname(os.path.realpath(sys.argv[0]))


def convert_file(fromYaml, toJson):
    with open(fromYaml, 'r') as stream:
        try:
            obj = yaml.load(stream)
            params = obj.get('parameters')
            if params is None:
                return

            parameters_array = []

            for key in params.keys():
                param_item = dict()
                param_item["ParameterKey"] = key
                param_item["ParameterValue"] = params.get(key)
                parameters_array.append(param_item)

            if len(parameters_array) > 0:
                jsonStr = json.dumps(parameters_array, indent=4, sort_keys=True)
                with open(toJson, 'w') as f:
                    f.write(jsonStr)

        except yaml.YAMLError as exc:
            print(exc)


def main():
    pwd = get_script_path()
    print(pwd)
    config_dir = os.path.join(pwd, 'config')
    for root, dirs, files in os.walk(config_dir, topdown=False):
        for name in files:
            yaml_file_name = os.path.join(root, name)
            if yaml_file_name.endswith('.yaml'):
                json_file_name = yaml_file_name[:-5] + '.json'
                print(yaml_file_name)
                convert_file(yaml_file_name, json_file_name)


if __name__ == '__main__':
    main()
